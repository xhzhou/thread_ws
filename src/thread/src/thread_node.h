/*
 * @Date: 2023-11-09 22:52:48
 * @LastEditors: Xiaohui.Zhou
 * @LastEditTime: 2023-11-19 16:51:31
 * @FilePath: /thread_ws/src/thread/src/thread_node.h
 * @Description:
 * Copyright (c) 2023 by ${git_name}, All Rights Reserved.
 */
#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>

struct PDATA {
  ros::Time timestamp;
  pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud;
};

struct CDATA {
  ros::Time timestamp;
  sensor_msgs::Image image;
};

#endif  // STRUCTURES_H